import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'FinderClub',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
